﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lv_1
{
	class Program
	{
		static void Main(string[] args)
		{
			Note homework = new Note( "Petar","Do your homework!", 3);
			Note dogWalking = new Note("Ivana","Walk the dog!");
			Note emptyNote = new Note();
			DateTime birthdayDate = new DateTime(1999, 6, 2, 11, 30, 52);
			TimeNote currentNote = new TimeNote("Petar", "My birthday!", 3, birthdayDate);

			Console.WriteLine(homework.ToString());
			Console.WriteLine(dogWalking.ToString());
			Console.WriteLine(emptyNote.ToString());
			Console.WriteLine(currentNote.ToString());
		}
	}
}
