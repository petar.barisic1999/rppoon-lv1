﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lv_1
{
	class TimeNote : Note
	{
		private DateTime time;
		public TimeNote() : base() {this.time= DateTime.Now;}
		public TimeNote(String author, String text, DateTime time )
			: base(author, text) { this.time = time; }
		public TimeNote(String author, String text, int levelOfImportance, DateTime time)
			:base (author, text, levelOfImportance) { this.time = time; }

		public DateTime Time {
			get { return this.time; }
			set { this.time = value; }

		}
		public override string ToString()
		{
			return base.ToString()+", " + this.time;
		}
	}
}
