﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lv_1
{
	class Note
	{
		private String text;
		private String author;
		private int levelOfImportance;


		public Note()
		{
			this.text = "N/A";
			this.author = "N/A";
			this.levelOfImportance = 0;

		}
		public Note (String author, String text)
		{
			this.text = text;
			this.author = author;
			this.levelOfImportance = 0;
		}
		public Note (String author, String text,int levelOfImportance)
		{
			this.text = text;
			this.author = author;
			this.levelOfImportance = levelOfImportance;
		}
        public string getText() { return this.text; }
		public string getAuthor() { return this.author; }
		public int getLevelOfImportance() { return this.levelOfImportance; }
		public void setText(String text) { this.text = text; }
		private void setAuthor(String author) { this.author = author; }
		public void setLevelOfImportance(int levelOfImportance) { this.levelOfImportance = levelOfImportance; }

		public string Text {
			get { return this.text; }
			set { this.text = value;}
		}
		public string Author {
			get { return this.author; }
			private set { this.author = value; }
		}
		public int LevelOfImportance {
			get { return this.levelOfImportance; }
			set { this.levelOfImportance = value; }
		}

		public override string ToString()
		{
			return this.author + ", " + this.text;
		}

	}
}
